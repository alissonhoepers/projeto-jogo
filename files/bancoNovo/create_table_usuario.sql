﻿create table Usuario (
	id serial,
	cpf character varying (50) not null,
	unidadeConsumidora character varying (30) not null,
	dsLogin character varying (240) not null,
	dsSenha character varying (50) not null,
	PRIMARY KEY (id)
);