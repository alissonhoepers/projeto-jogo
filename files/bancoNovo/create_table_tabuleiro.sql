﻿create table Tabuleiro (
	id serial,
	idPergunta int,
	idDicas int,
	idFase int,
	PRIMARY KEY (id),
	FOREIGN KEY (idPergunta) REFERENCES Perguntas(id),
	FOREIGN KEY (idDicas) REFERENCES Dicas(id),
	FOREIGN KEY (idFase) REFERENCES Fase(id)
)