﻿create table Resposta (
	id serial,
	descricao character varying (240) not null,
	flagCorreta boolean not null,
	idPergunta int,
	PRIMARY KEY (id),
	FOREIGN KEY (idPergunta) REFERENCES Perguntas(id)
);
	
	