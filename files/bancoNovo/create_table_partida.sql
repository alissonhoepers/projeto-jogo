﻿create table Partida (
	id serial,
	idTabuleiro int not null,
	idUsuario int not null, 
	dataInicio timestamp not null,
	dataFinalizou timestamp,
	finalizado boolean not null,
	pontos bigint,
	PRIMARY KEY (id),
	FOREIGN KEY (idTabuleiro) REFERENCES Tabuleiro(id),
	FOREIGN KEY (idUsuario) REFERENCES Usuario(id)
)
	