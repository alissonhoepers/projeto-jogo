package br.com.senai.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	private String urlDeConexao = "jdbc:postgresql://localhost:5432/projetoJogo";
	private String usuario = "postgres";
	private String senha = "postgres";
	private String drive= "org.postgresql.Driver";

	public Connection getConnection(){
		
		Connection conexao = null;
		
		try {
			Class.forName(drive);
			conexao = DriverManager.getConnection(urlDeConexao, usuario, senha);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		return conexao;
		
	}

}
