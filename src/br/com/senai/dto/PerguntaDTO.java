package br.com.senai.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.senai.model.Resposta;

@Entity
@Table(name = "perguntas" )
@SequenceGenerator(name = "perguntas_seq", sequenceName = "perguntas_id_seq", allocationSize = 1, initialValue = 1)
public class PerguntaDTO extends DTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="perguntas_seq", strategy=GenerationType.SEQUENCE)
	private Integer id;
	@Column
	private String descricao;
	@Column
	private String bonus;
	
	@Column
	private String dicas;
	
	@Column
	private String imagem;
	
	@ManyToOne
	@JoinColumn(name="idfase", 	referencedColumnName="id")
	private FaseDTO fase;
	
	@OneToMany(mappedBy="pergunta")
	private List<RespostaDTO> respostas;
	
	@Transient
	private RespostaDTO respostaSelecionada;
	
	@Transient
	private int numero;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getBonus() {
		return bonus;
	}
	public void setBonus(String bonus) {
		this.bonus = bonus;
	}
	public FaseDTO getFase() {
		return fase;
	}
	public void setFase(FaseDTO fase) {
		this.fase = fase;
	}

	public String getDicas() {
		return dicas;
	}

	public void setDicas(String dicas) {
		this.dicas = dicas;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	@Override
	public String toString() {
		return "p"+id;
	}

	public List<RespostaDTO> getRespostas() {
		return respostas;
	}

	public void setRespostas(List<RespostaDTO> respostas) {
		this.respostas = respostas;
	}

	public RespostaDTO getRespostaSelecionada() {
		return respostaSelecionada;
	}

	public void setRespostaSelecionada(RespostaDTO respostaSelecionada) {
		this.respostaSelecionada = respostaSelecionada;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
	

	
}
