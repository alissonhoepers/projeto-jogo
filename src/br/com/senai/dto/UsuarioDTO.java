package br.com.senai.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
@SequenceGenerator(name = "usuario_seq", sequenceName = "usuario_id_seq", allocationSize = 1, initialValue = 1)
public class UsuarioDTO extends DTO implements Serializable{

	private static final long serialVersionUID = 1L;

	public UsuarioDTO() {
		super();
	}
	
	@Id
	@GeneratedValue(generator="usuario_seq", strategy=GenerationType.SEQUENCE)
	private Integer id;
	@Column
	private String cpf;
	@Column
	private String dsLogin;
	@Column
	private String dsSenha;
	@Column
	private String unidadeConsumidora;
	@Column
	private int pontos;
	@Column
	private String nome;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getDsLogin() {
		return dsLogin;
	}
	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}
	public String getDsSenha() {
		return dsSenha;
	}
	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}
	public String getUnidadeConsumidora() {
		return unidadeConsumidora;
	}
	public void setUnidadeConsumidora(String unidadeConsumidora) {
		this.unidadeConsumidora = unidadeConsumidora;
	}
	public int getPontos() {
		return pontos;
	}
	public void setPontos(int pontos) {
		this.pontos = pontos;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
