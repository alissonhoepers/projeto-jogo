package br.com.senai.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.senai.model.Pergunta;

@Entity
@Table(name="resposta")
@SequenceGenerator(name = "resposta_seq", sequenceName = "resposta_id_seq", allocationSize = 1, initialValue = 1)
public class RespostaDTO {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="resposta_seq", strategy=GenerationType.SEQUENCE)
	private Integer id;
	@Column
	private String descricao;
	@Column
	private boolean flagcorreta;
	@ManyToOne
	@JoinColumn(name="idPergunta", referencedColumnName="id")
	private PerguntaDTO pergunta;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public boolean isFlagcorreta() {
		return flagcorreta;
	}
	public void setFlagcorreta(boolean flagcorreta) {
		this.flagcorreta = flagcorreta;
	}
	public PerguntaDTO getPergunta() {
		return pergunta;
	}
	public void setPergunta(PerguntaDTO pergunta) {
		this.pergunta = pergunta;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RespostaDTO other = (RespostaDTO) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
