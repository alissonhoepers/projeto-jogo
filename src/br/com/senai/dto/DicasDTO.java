package br.com.senai.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="dicas")
@SequenceGenerator(name = "dica_seq", sequenceName = "dicas_id_seq", allocationSize = 1, initialValue = 1)
public class DicasDTO extends DTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public DicasDTO() {
		super();
	}

	@Id
	@GeneratedValue(generator="dica_seq", strategy=GenerationType.SEQUENCE)
	private Integer id;
	
	@Column
	private String dica;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDica() {
		return dica;
	}

	public void setDica(String dica) {
		this.dica = dica;
	}
	

}
