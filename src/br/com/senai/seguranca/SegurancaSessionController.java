package br.com.senai.seguranca;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.senai.model.Usuario;

@Named
@SessionScoped
public class SegurancaSessionController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Usuario usuarioLogado;

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
	
	public Boolean usuarioEstaLogado() {
		return usuarioLogado != null; 
	}
	
}
