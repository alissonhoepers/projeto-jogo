package br.com.senai.model;

public class Usuario {
	
	private int id;
	private String cpf;
	private String dsLogin;
	private String dsSenha;
	private String unidadeConsumidora;
	private Integer pontos;
	private String nome;
	
	

	public Usuario(int id, String cpf, String dsLogin, String dsSenha, String unidadeConsumidora, Integer pontos, String nome) {
		super();
		this.id = id;
		this.cpf = cpf;
		this.dsLogin = dsLogin;
		this.dsSenha = dsSenha;
		this.unidadeConsumidora = unidadeConsumidora;
		this.pontos = pontos;
		this.nome = nome;
	}
	
	public Usuario() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getDsLogin() {
		return dsLogin;
	}
	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}
	public String getDsSenha() {
		return dsSenha;
	}
	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}

	public String getUnidadeConsumidora() {
		return unidadeConsumidora;
	}

	public void setUnidadeConsumidora(String unidadeConsumidora) {
		this.unidadeConsumidora = unidadeConsumidora;
	}
	public int getPontos() {
		return pontos;
	}
	public void setPontos(Integer pontos) {
		this.pontos = pontos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
