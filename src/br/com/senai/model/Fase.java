package br.com.senai.model;

public class Fase {
	private int id;
	private String descricao;
	
	public Fase(int id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}
	public Fase() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
