package br.com.senai.model;

public class Tabuleiro {
	
	private int id;
	private int idPergunta;
	private int idDicas;
	private int idFase;
	public Tabuleiro(int id, int idPergunta, int idDicas, int idFase) {
		super();
		this.id = id;
		this.idPergunta = idPergunta;
		this.idDicas = idDicas;
		this.idFase = idFase;
	}
	public Tabuleiro() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdPergunta() {
		return idPergunta;
	}
	public void setIdPergunta(int idPergunta) {
		this.idPergunta = idPergunta;
	}
	public int getIdDicas() {
		return idDicas;
	}
	public void setIdDicas(int idDicas) {
		this.idDicas = idDicas;
	}
	public int getIdFase() {
		return idFase;
	}
	public void setIdFase(int idFase) {
		this.idFase = idFase;
	}
	
	

}
