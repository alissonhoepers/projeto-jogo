package br.com.senai.model;

public class Dica {
	
	private int id;
	private String dica;
	public Dica() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Dica(int id, String dica) {
		super();
		this.id = id;
		this.dica = dica;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDica() {
		return dica;
	}
	public void setDica(String dica) {
		this.dica = dica;
	}
	
	

}
