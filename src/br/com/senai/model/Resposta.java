package br.com.senai.model;

public class Resposta {
	
	private int id;
	private String descricao;
	private boolean flagCorreta;
	private int idPergunta;
	public Resposta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Resposta(int id, String descricao, boolean flagCorreta, int idPergunta) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.flagCorreta = flagCorreta;
		this.idPergunta = idPergunta;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public boolean isFlagCorreta() {
		return flagCorreta;
	}
	public void setFlagCorreta(boolean flagCorreta) {
		this.flagCorreta = flagCorreta;
	}
	public int getIdPergunta() {
		return idPergunta;
	}
	public void setIdPergunta(int idPergunta) {
		this.idPergunta = idPergunta;
	}
	@Override
	public String toString() {
		return "Resposta [id=" + id + "]";
	}
	
	

}
