package br.com.senai.model;

public class Pergunta {
	
	private int id;
	private String descricao;
	private String bonus;
	private String onus;
	private int idFase;
	public Pergunta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Pergunta(int id, String descricao, String bonus, String onus, int idFase) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.bonus = bonus;
		this.onus = onus;
		this.idFase = idFase;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getBonus() {
		return bonus;
	}
	public void setBonus(String bonus) {
		this.bonus = bonus;
	}
	public String getOnus() {
		return onus;
	}
	public void setOnus(String onus) {
		this.onus = onus;
	}
	public int getIdFase() {
		return idFase;
	}
	public void setIdFase(int idFase) {
		this.idFase = idFase;
	}
	
	

}
