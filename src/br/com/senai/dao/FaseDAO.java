package br.com.senai.dao;

import java.util.List;

import org.hibernate.Session;

import br.com.senai.dto.FaseDTO;
import br.com.senai.dto.PerguntaDTO;

public class FaseDAO extends DAO{

	public void salvar(FaseDTO faseDto) {
		// TODO Auto-generated method stub
		
	}
	
	public FaseDTO buscarPorId(int idFase) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<FaseDTO> listar() {
		
		List<FaseDTO> fases = null;
		
		Session session = getSession();
	
		
		fases = session.createQuery("select f from FaseDTO f order by f,id ")
					   .list();
		int i = 0;
		for (FaseDTO faseDTO : fases) {
			faseDTO.getPerguntas().size();
			i = 0;
			for (PerguntaDTO p : faseDTO.getPerguntas()) {
				p.getRespostas().size();
				p.setNumero(++i);
			}
		}
		
		session.clear();
		
		return fases;
	}

}
