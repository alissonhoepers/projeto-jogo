package br.com.senai.dao;

import org.hibernate.Session;

import br.com.senai.dto.DTO;

public abstract class DAO {

	protected Session getSession(){
		return Hibernate.getSession();
	}

	//---------------------------------------------------------------------
	public void begin() {
		getSession().beginTransaction();
	}
	
	public void commit() {
		getSession().getTransaction().commit();
	}
	
	public void rollback() {
		getSession().getTransaction().rollback();
	}
	
	public void close() {
		if (getSession().isOpen()) {
			getSession().close();
			getSession().clear();
		}
	}
	
	//---------------------------------------------------------------------
	public void salvar(DTO tabela) {
		begin();
		getSession().save(tabela);
		commit();
	}
	
	public void atualizar(DTO tabela) {
		begin();
		getSession().merge(tabela);
		commit();
	}	
	
	public void excluir(DTO tabela) {
		begin();
		getSession().delete(tabela);
		commit();
	}
	
}
