package br.com.senai.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Hibernate {
	
	private static Session session;

	public static final String arquivoConfiguracao = "/br/com/senai/dao/hibernate.cfg.xml";
	
	private static SessionFactory sessionFactory() {
		Configuration cfg = new Configuration();
		cfg.configure(arquivoConfiguracao);
		return cfg.buildSessionFactory();
	}
	
	public static Session getSession() {
		if (session == null) {
			session = sessionFactory().openSession();
		}
		return session;
	}
	
}
