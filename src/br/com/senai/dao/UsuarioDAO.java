package br.com.senai.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//import org.postgresql.core.ConnectionFactory;

import br.com.senai.model.Usuario;
import br.com.senai.util.ConnectionFactory;

public class UsuarioDAO extends DAO{
	
	public List<Usuario> listarUsuarios() {
		String sql = "SELECT id, cpf, dslogin, dssenha FROM usuario";

		try {
			Connection conexao = new ConnectionFactory().getConnection();
			PreparedStatement preparedStatement = conexao.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			return populaUsuario(resultSet);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	private List<Usuario> populaUsuario(ResultSet resultSet) {
		System.err.println(populaUsuario(resultSet));

		List<Usuario> usuarios = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Usuario c = new Usuario();
				c.setId(resultSet.getInt("id"));
				c.setCpf(resultSet.getString("cpf"));
				c.setDsLogin(resultSet.getString("dslogin"));
				c.setDsSenha(resultSet.getString("dssenha"));
				usuarios.add(c);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return usuarios;
	}
	
	public Usuario consultaUsuario(String login, String senha){
		String sql = "SELECT id, nome, unidadeconsumidora, cpf, dslogin, dssenha, pontos FROM usuario WHERE dslogin = ? AND dssenha = ?";
		
		try {
			Connection conexao = new ConnectionFactory().getConnection();
			PreparedStatement preparedStatement = conexao.prepareStatement(sql);
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, senha);
			ResultSet resultSet = preparedStatement.executeQuery();
			return usuario(resultSet);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	
	}
	
	private Usuario usuario(ResultSet resultSet) {

		Usuario c = null;

		try {
			while (resultSet.next()) {
				c = new Usuario();
				c.setId(resultSet.getInt("id"));
				c.setCpf(resultSet.getString("cpf"));
				c.setDsLogin(resultSet.getString("dslogin"));
				c.setDsSenha(resultSet.getString("dssenha"));
				c.setPontos(resultSet.getInt("pontos"));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return c;
	}
	
	public void inserir(Usuario usuario) {

		String sql = "INSERT INTO usuario (id, cpf, unidadeconsumidora, dslogin, dssenha ) VALUES (?, ?, ?, ?)";

		try {
			Connection connection = new ConnectionFactory().getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, usuario.getCpf());
			preparedStatement.setString(2, usuario.getUnidadeConsumidora());
			preparedStatement.setString(3, usuario.getDsLogin());
			preparedStatement.setString(4, usuario.getDsSenha());
			preparedStatement.execute();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void editar(Usuario usuario) {

		String sql = "UPDATE usuario SET cpf = ?, unidadeconsumidora = ?, dslogin = ?, dssenha = ? WHERE id = ?";

		try {
			Connection connection = new ConnectionFactory().getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, usuario.getCpf());
			preparedStatement.setString(2, usuario.getUnidadeConsumidora());
			preparedStatement.setString(3, usuario.getDsLogin());
			preparedStatement.setString(4, usuario.getDsSenha());
			
			preparedStatement.setInt(5, usuario.getId());
			preparedStatement.execute();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void excluir(Usuario usuario) {

		String sql = "DELETE FROM usuario WHERE id = ?";

		try {
			Connection connection = new ConnectionFactory().getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			// preparedStatement.setString(1, usuario.getNome());
			preparedStatement.setInt(1, usuario.getId());
			preparedStatement.execute();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void addPontos(Usuario usuario) {
		String sql = "UPDATE usuario SET pontos = ? WHERE id = ?";

		try {
			Connection connection = new ConnectionFactory().getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, usuario.getPontos());
			preparedStatement.setInt(2, usuario.getId());
			preparedStatement.execute();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
