package br.com.senai.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.senai.controller.DicasController;
import br.com.senai.dto.DicasDTO;

@Named
@SessionScoped
public class DicasMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DicasController dicasController;
	private DicasDTO dicasDTO;
	private List<DicasDTO> listaDicas;
	
	@PostConstruct
	public void inicializa(){
		dicasDTO = new DicasDTO();
		listaDicas = new ArrayList<DicasDTO>();
		lista();
	}

	private void lista() {
		List<DicasDTO> lista = dicasController.listaDicas();
		
		for (DicasDTO dicas : lista) {
			listaDicas.add(dicas);
		}
	}

	public List<DicasDTO> getListaDicas() {
		return listaDicas;
	}

	public void setListaDicas(List<DicasDTO> listaDicas) {
		this.listaDicas = listaDicas;
	}
}
