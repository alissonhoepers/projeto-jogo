package br.com.senai.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.senai.controller.PerguntaController;
import br.com.senai.controller.RespostaController;
import br.com.senai.dao.FaseDAO;
import br.com.senai.dto.FaseDTO;
import br.com.senai.dto.PerguntaDTO;
import br.com.senai.dto.RespostaDTO;

@Named
@SessionScoped
public class PerguntaMB implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	private PerguntaController perguntaController;
	private RespostaController respostaController;
	private PerguntaDTO perguntaDTO;
	private List<PerguntaDTO> listaPergunta;
	private List<RespostaDTO> listaResposta;
	
	@PostConstruct
	public void inicializa() {
		perguntaDTO = new PerguntaDTO();
		listaPergunta = new ArrayList<>();
	}

	public List<PerguntaDTO> listaPerguntas(int idFase) {
		FaseDAO faseDAO = new FaseDAO();
		FaseDTO faseDTO = faseDAO.buscarPorId(idFase);
		List<PerguntaDTO> lista = new ArrayList<PerguntaDTO>();
		
		for (PerguntaDTO pergunta : perguntaController.listaPergunta()) {
			if (pergunta.getFase().equals(faseDTO)){
				lista.add(pergunta);
			}
		}
		
		return lista;
	}
	
	public List<RespostaDTO> listaResposta(PerguntaDTO pergunta) {
		List<RespostaDTO> lista = new ArrayList<RespostaDTO>();
		
		for (RespostaDTO resposta : respostaController.listaResposta()) {
			if (resposta.getPergunta().equals(pergunta)){
				lista.add(resposta);
			}
		}
		return lista;
	}


}
