package br.com.senai.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class FinalMB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int acertos;

	public int getAcertos() {
		return acertos;
	}

	public void setAcertos(int acertos) {
		this.acertos = acertos;
	}
	
	
}
