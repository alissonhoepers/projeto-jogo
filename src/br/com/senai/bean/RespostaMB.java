package br.com.senai.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.senai.controller.RespostaController;
import br.com.senai.dto.RespostaDTO;

@Named
@ViewScoped
public class RespostaMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private RespostaController respostaController;
	private RespostaDTO respostaDTO;
	private List<RespostaDTO> listaResposta;
	
	@PostConstruct
	public void inicializar() {
		respostaDTO = new RespostaDTO();
		listaResposta = new ArrayList<RespostaDTO>();
		lista();
	}

	private void lista() {
		List<RespostaDTO> lista = respostaController.listaResposta();
		
		for (RespostaDTO resposta : lista) {
			listaResposta.add(resposta);
		}
	}
	

}
