package br.com.senai.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.senai.controller.LoginController;
import br.com.senai.controller.PerguntaController;
import br.com.senai.dao.UsuarioDAO;
import br.com.senai.dto.FaseDTO;
import br.com.senai.dto.PerguntaDTO;
import br.com.senai.dto.RespostaDTO;
import br.com.senai.model.Usuario;
import br.com.senai.seguranca.SegurancaSessionController;

@Named
@ViewScoped
public class JogoMB implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PerguntaController controller;
	
	private List<FaseDTO> fases;
	
	private FaseDTO fase;
	
	private PerguntaDTO pergunta;
	
	private UsuarioDAO usuarioDao;
	
	@Inject
	private SegurancaSessionController sessao;

	
	@Inject
	private FinalMB finalMb;
	
	@PostConstruct
	public void inicializar(){
		controller = new PerguntaController();
		fases = controller.listaFases();
		usuarioDao = new UsuarioDAO();
		proximaFase();
	}
	
	public void proximaFase() {
		int numeroFase =  fases.indexOf(fase)+1;
		if(fases != null || !fases.isEmpty()){
			if(numeroFase+1 > fases.size()){
				calcularPontuacao();
				Usuario usuario = sessao.getUsuarioLogado();
				usuario.setPontos(usuario.getPontos() + finalMb.getAcertos());
				usuarioDao.addPontos(usuario);
				//dar update dos pontos no banco aqui
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("/projeto-jogo/final.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				fase = fases.get(numeroFase);
			}
		}
		
	}

	public void calcularPontuacao() {
		int questoesCorretas = 0;
		for (FaseDTO faseDTO : fases) {
			for (PerguntaDTO pergunta : faseDTO.getPerguntas()) {
				for (RespostaDTO resposta : pergunta.getRespostas()) {
					if(resposta.isFlagcorreta() && resposta.equals(pergunta.getRespostaSelecionada())){
						questoesCorretas++;
					}
				}
			}
		}
		//aqui tens o total correto.
		finalMb.setAcertos(questoesCorretas);
	}

	public FaseDTO getFase() {
		return fase;
	}
	
	public List<FaseDTO> getFases() {
		return fases;
	}

	public PerguntaDTO getPergunta() {
		if(pergunta == null){
			pergunta = new PerguntaDTO();
		}
		
		return pergunta;
	}

	public void setPergunta(PerguntaDTO pergunta) {
		this.pergunta = pergunta;
	}
	
	public void selecionarPergunta(PerguntaDTO perguntaDTO){
		this.pergunta = perguntaDTO;
	}

}
