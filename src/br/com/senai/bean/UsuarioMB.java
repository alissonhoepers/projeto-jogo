package br.com.senai.bean;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.senai.controller.LoginController;
import br.com.senai.controller.UsuarioController;
import br.com.senai.dto.UsuarioDTO;

@Named
@ViewScoped
public class UsuarioMB implements Serializable{

	private static final long serialVersionUID = 1L;

	private UsuarioController usuarioController;
	private LoginController loginController;
	private UsuarioDTO usuarioDTO;
	
	@PostConstruct
	public void inicializar() {
		usuarioController = new UsuarioController();
		loginController = new LoginController();
		usuarioDTO = new UsuarioDTO();
	}
	
	public void salvar() {
		
		usuarioController.salvar(usuarioDTO);
		
	}

	public UsuarioDTO getUsuarioDTO() {
		return usuarioDTO;
	}

	public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
		this.usuarioDTO = usuarioDTO;
	}
	
}