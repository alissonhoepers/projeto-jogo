package br.com.senai.exception;

import java.io.IOException;

public class LoginIncorretoException extends IOException{

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Usu�rio e Senha est�o incorretos.";
	}
}
