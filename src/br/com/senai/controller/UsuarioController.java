package br.com.senai.controller;

import br.com.senai.dao.UsuarioDAO;
import br.com.senai.dto.UsuarioDTO;

public class UsuarioController {
	
	private UsuarioDAO usuarioDAO;
	
	public UsuarioController() {
		this.usuarioDAO = new UsuarioDAO();
	}

	public void salvar(UsuarioDTO usuarioDTO) {
		usuarioDAO.salvar(usuarioDTO);
	}

}
