package br.com.senai.controller;

import java.util.List;

import br.com.senai.dao.FaseDAO;
import br.com.senai.dao.PerguntaDAO;
import br.com.senai.dto.FaseDTO;
import br.com.senai.dto.PerguntaDTO;

public class PerguntaController {
	
	private PerguntaDAO perguntaDAO;
	
	private FaseDAO faseDAO;
	
	public PerguntaController() {
		faseDAO = new FaseDAO();
		perguntaDAO = new PerguntaDAO();
	}

	public List<PerguntaDTO> listaPergunta() {
		return perguntaDAO.listar();
	}

	public List<FaseDTO> listaFases() {
		return faseDAO.listar();
	}

}
