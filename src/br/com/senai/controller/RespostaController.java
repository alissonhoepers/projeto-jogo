package br.com.senai.controller;

import java.util.List;

import br.com.senai.dao.RespostaDAO;
import br.com.senai.dto.RespostaDTO;

public class RespostaController {

	private RespostaDAO respostaDAO;
	
	public List<RespostaDTO> listaResposta() {
		return respostaDAO.listar();
	}

}
