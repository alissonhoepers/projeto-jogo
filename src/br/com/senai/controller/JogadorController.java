package br.com.senai.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.senai.model.Usuario;

@Named(value = "JogadorController")
@SessionScoped
public class JogadorController implements Serializable {

	private static final long serialVersionUID = 1L;

	private Usuario usuario;

	@PostConstruct
	public void inicializar() {

		usuario = new Usuario();

	}

	public Usuario getJogador() {
		return usuario;
	}

	public void setJogador(Usuario usuario) {
		this.usuario = usuario;
	}
}