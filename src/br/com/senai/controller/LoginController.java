package br.com.senai.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.senai.dao.UsuarioDAO;
import br.com.senai.exception.LoginIncorretoException;
import br.com.senai.model.Usuario;
import br.com.senai.seguranca.SegurancaSessionController;


@Named
@SessionScoped
public class LoginController implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String usuario;
	private String senha;
	private UsuarioDAO usuarioDao;
	
	@Inject
	private SegurancaSessionController sessao;
	
	@PostConstruct
	public void inicializa(){
		usuarioDao = new UsuarioDAO();
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public void logar() throws IOException {
		try {
			Usuario usuario = usuarioDao.consultaUsuario(this.usuario, this.senha);
			if (usuario != null) {
				sessao.setUsuarioLogado(usuario);
				FacesContext.getCurrentInstance().getExternalContext().redirect("/projeto-jogo/principal.xhtml");
			} else {
				FacesContext.getCurrentInstance().addMessage("Login Incorreto", new FacesMessage("O e-mail ou senha est�o incorretos."));
			}
		} catch (Exception e) {
			e.getMessage();
		}

	}
	
}
