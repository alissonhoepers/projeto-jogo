package br.com.senai.controller;

import java.util.List;

import br.com.senai.dao.DicasDAO;
import br.com.senai.dto.DicasDTO;

public class DicasController {

	private DicasDAO dicasDAO;

	public DicasController() {
		this.dicasDAO = new DicasDAO();
	}

	public List<DicasDTO> listaDicas() {
		return dicasDAO.listar();
	}
	
	
}
